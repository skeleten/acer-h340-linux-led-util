#include <sys/types.h>
#include <sys/io.h>
#include <unistd.h>
#include <cstdio>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>
#include <list>

#include "defines.h"
// #include "funcs.h"
#include "diskInfos.h"
#include "ledDriver.h"
#define null NULL

using namespace std;
using namespace skeleten;

list<diskInfo*>* getInfos();
void clearList(list<diskInfo*>* l);

int main(int argc, char** argv)
{
    list<diskInfo*> *dInfos = new list<diskInfo*>();
    list<diskInfo*>::iterator it;
    ledDriver* driver = new ledDriver();
    diskInfo
      *hd1 = null,
      *hd2 = null,
      *hd3 = null,
      *hd4 = null;

    if (!driver->init())
    {
        cerr << "couldn't grant access to the IO-ports. Are you root?" << endl;
        return -1;
    }

    /** run in background */
    if (fork()) return 0;

    while (true)
    {
        clearList(dInfos);
        delete dInfos;
        dInfos = getInfos();

        hd1 = null;
        hd2 = null;
        hd3 = null;
        hd4 = null;

        for(it = dInfos->begin(); it != dInfos->end(); it++)
        {
          switch((*it)->host)
          {
            case 0:
              hd1 = *it;
              break;
            case 1:
              hd2 = *it;
              break;
            case 2:
              hd3 = *it;
              break;
            case 3:
              hd4 = *it;
              break;
            default:
              break;
          }
        }
        driver->resetAll();

        if(hd1)
        {
          if(hd1->busy)
            driver->setHd1Red(true);
          else
	    driver->setHd1Blu(true);
        }
        if(hd2)
	{
          if(hd2->busy)
            driver->setHd2Red(true);
	  else
	    driver->setHd2Blu(true);
	}
        if(hd3)
	{
          if(hd3->busy)
	    driver->setHd3Red(true); 
	  else
	    driver->setHd3Blu(true);
	}
        if(hd4)
	{
          if(hd4->busy)
	    driver->setHd4Red(true);
	  else
	    driver->setHd4Blu(true);
	}

        driver->update();
        sleep(SLEEP);
    };

    clearList(dInfos);
    delete dInfos;
    // delete ledDriver;
    return 0; // never reached
}

list<diskInfo*>* getInfos()
{
  list<diskInfo*>* infos = new list<diskInfo*>();
  ifstream stream;
  stream.open(DISK_INFO, ios::in);
  while(!stream.eof())
  {
    diskInfo* info = new diskInfo();
    info->read(stream);
    infos->push_back(info);
  }
  return infos;
};

void clearList(std::list< diskInfo* >* l)
{
  while(l->size() > 0)
  {
    diskInfo* cur = l->front();
    l->pop_front();
    delete cur;
  }
}
