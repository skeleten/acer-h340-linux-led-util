#ifndef _DISK_INFOS_H
#define _DISK_INFOS_H

#include <iostream>
#include <fstream>

namespace skeleten
{
  class diskInfo
  {
  public:
    diskInfo();
    int host;
    int chan;
    int id;
    int lun;
    int type;
    int opens;
    int qdepth;
    int busy;
    int online;
    
    void read(std::istream& stream);
  };
};

#endif