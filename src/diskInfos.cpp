#ifndef _DISK_INFOS_CPP
#define _DISK_INFOS_CPP
#include "diskInfos.h"
#include <iostream>

namespace skeleten
{
  diskInfo::diskInfo()
  {
  }

  void diskInfo::read(std::istream& stream)
  {
    stream
      >> this->host
      >> this->chan
      >> this->id
      >> this->lun
      >> this->type
      >> this->opens
      >> this->qdepth
      >> this->busy
      >> this->online;
  }
};
#endif
