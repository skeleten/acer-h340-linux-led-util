#ifndef _LED_DRIVER_CPP
#define _LED_DRIVER_CPP
#include "ledDriver.h"
#include <sys/io.h>

namespace skeleten
{
    ledDriver::ledDriver()
    {
        this->val_gp1 = 0x0;
        this->val_gp5 = 0x0;
    };
    void ledDriver::setHd1Blu(bool on)
    {
        if(on)
            this->val_gp5 |= this->hd1Blu;
        else
            this->val_gp5 &= ~this->hd1Blu;
    };
    void ledDriver::setHd2Blu(bool on)
    {
        if(on)
            this->val_gp5 |= this->hd2Blu;
        else
            this->val_gp5 &= ~ this->hd2Blu;
    };
    void ledDriver::setHd3Blu(bool on)
    {
        if(on)
            this->val_gp5 |= this->hd3Blu;
        else
            this->val_gp5 &= ~this->hd3Blu;
    };
    void ledDriver::setHd4Blu(bool on)
    {
        if(on)
            this->val_gp1 |= this->hd4Blu;
        else
            this->val_gp1 &= ~this->hd4Blu;
    };
    void ledDriver::setHd1Red(bool on)
    {
        if(on)
            this->val_gp5 |= this->hd1Red;
        else
            this->val_gp5 &= ~this->hd1Red;
    };
    void ledDriver::setHd2Red(bool on)
    {
        if(on)
            this->val_gp5 |= this->hd2Red;
        else
            this->val_gp5 &= ~this->hd2Red;
    };
    void ledDriver::setHd3Red(bool on)
    {
      if(on)
            this->val_gp5 |= this->hd3Red;
        else
            this->val_gp5 &= this->hd3Red;
    };
    void ledDriver::setHd4Red(bool on)
    {
        if(on)
            this->val_gp1 |= this->hd4Red;
        else
            this->val_gp1 &= ~this->hd4Red;
    }
    bool ledDriver::init()
    {
        return !(ioperm(BASEIO, BASEIO_SZ, 1));
    };
    void ledDriver::update()
    {
        outb(this->val_gp1, BASEIO+GP1);
        outb(this->val_gp5, BASEIO+GP5);
    };
    void ledDriver::resetAll()
    {
        this->val_gp1 = 0x0;
        this->val_gp5 = 0x0;
    };
}

#endif
