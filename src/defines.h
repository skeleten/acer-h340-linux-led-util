#ifndef _DEFINES_H
#define _DEFINES_H

#define DISK_INFO "/proc/scsi/sg/devices"

/** time to wait for next update */
#define SLEEP 2

#endif
