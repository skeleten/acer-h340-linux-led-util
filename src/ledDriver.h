#ifndef _LED_DRIVER_H
#define _LED_DRIVER_H

namespace skeleten
{
    class ledDriver
    {
    public:
        ledDriver();
        bool init();
        void update();
        void setHd1Blu(bool);
        void setHd2Blu(bool);
        void setHd3Blu(bool);
        void setHd4Blu(bool);
        void setHd1Red(bool);
        void setHd2Red(bool);
        void setHd3Red(bool);
        void setHd4Red(bool);
        void resetAll();
    private:
        char val_gp1, val_gp5;
        const char
            hd1Blu = (1 << 6),
            hd2Blu = (1 << 2),
            hd3Blu = (1 << 0),
            hd4Blu = (1 << 4),
            hd1Red = (1 << 7),
            hd2Red = (1 << 3),
            hd3Red = (1 << 1),
            hd4Red = (1 << 1);

    };
}

/***********************
 * Other value defines *
 ***********************/
#define BASEIO 0x800
#define BASEIO_SZ 0x7F

#define GP1 0x4b
#define GP5 0x4f

#endif
